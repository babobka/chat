package org.example.handler;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import org.example.model.Image;

import java.util.Base64;

import static org.example.model.Image.Status.OK;

/**
 * Created by 123 on 26.03.2018.
 */
public class ImageResultReaderHandler implements Handler<AsyncResult<Message<String>>> {

    private final RoutingContext request;

    public ImageResultReaderHandler(RoutingContext request) {
        this.request = request;
    }

    @Override
    public void handle(AsyncResult<Message<String>> result) {
        Image image = Json.decodeValue(result.result().body(), Image.class);
        if (image.getStatus() == OK) {
            request.response()
                    .putHeader("Content-Type", image.getContentType())
                    .end(Buffer.buffer(Base64.getDecoder().decode(image.getBase64Data())));
        } else {
            request.response().setStatusCode(404).end("file not found");
        }
    }
}