package org.example.handler;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import org.example.model.Image;
import org.example.model.ImageFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Set;

/**
 * Created by 123 on 25.03.2018.
 */
public class ImageUploaderHandler implements Handler<RoutingContext> {
    private final Vertx vertx;

    public ImageUploaderHandler(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void handle(RoutingContext request) {
        Set<FileUpload> fileUploadSet = request.fileUploads();
        try {
            Image image = readFile(fileUploadSet);
            if (image == null) {
                request.response().setStatusCode(500).end("no file was uploaded");
            } else {
                vertx.eventBus().send("images.save", image.toString(), result ->
                        request.response().end(result.result().body().toString()));
            }
        } finally {
            removeTempFiles(fileUploadSet);
        }
    }

    private Image readFile(Set<FileUpload> fileUploadSet) {
        for (FileUpload file : fileUploadSet) {
            Buffer uploadedFile = vertx.fileSystem().readFileBlocking(file.uploadedFileName());
            String fileName = getFileName(file);
            if (fileName == null) {
                continue;
            }
            String base64Content = Base64.getEncoder().encodeToString(uploadedFile.getBytes());
            return ImageFactory.ok(fileName, base64Content);
        }
        return null;
    }

    private void removeTempFiles(Set<FileUpload> fileUploadSet) {
        for (FileUpload file : fileUploadSet) {
            try {
                Files.delete(Paths.get(file.uploadedFileName()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String getFileName(FileUpload fileUpload) {
        try {
            return URLDecoder.decode(fileUpload.fileName(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
