package org.example.handler;

import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

/**
 * Created by 123 on 25.03.2018.
 */
public class ImageReaderHandler implements Handler<RoutingContext> {

    private final Vertx vertx;

    public ImageReaderHandler(Vertx vertx) {
        this.vertx = vertx;
    }

    @Override
    public void handle(RoutingContext request) {
        String imageId = request.queryParams().get("imageId");
        vertx.eventBus().send("images.read", imageId, new ImageResultReaderHandler(request));
    }

}
