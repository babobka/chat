package org.example.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import org.example.handler.ImageReaderHandler;
import org.example.handler.ImageUploaderHandler;

public class ImageRestServerVerticle extends AbstractVerticle {
    @Override
    public void start() {
        HttpServer httpServer = vertx.createHttpServer();
        Router httpRouter = Router.router(vertx);
        httpRouter.route().handler(BodyHandler.create());
        httpRouter.get("/readImage")
                .handler(new ImageReaderHandler(vertx));
        httpRouter.post("/uploadImage")
                .handler(new ImageUploaderHandler(vertx));
        httpRouter.get("/uploadImage")
                .handler(request -> request.response().sendFile("static/upload.html"));
        httpServer.requestHandler(httpRouter::accept);
        httpServer.listen(8083);
    }

}
