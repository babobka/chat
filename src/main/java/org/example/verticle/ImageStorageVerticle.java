package org.example.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import org.example.model.ImageUploadResult;
import org.example.model.Image;
import org.example.model.ImageFactory;

import java.util.List;

/**
 * Created by 123 on 26.03.2018.
 */
public class ImageStorageVerticle extends AbstractVerticle {
    private MongoClient client;

    @Override
    public void start() {
        client = MongoClient.createShared(vertx, new JsonObject().put("db_name", "my_DB"));
        vertx.eventBus().consumer("images.save", this::saveImage);
        vertx.eventBus().consumer("images.read", this::readImage);
    }

    private void saveImage(Message<String> message) {
        Image image = Json.decodeValue(message.body(), Image.class);
        client.insert("image", new JsonObject(message.body()), event -> {
            ImageUploadResult uploadResult = new ImageUploadResult();
            uploadResult.setId(event.result());
            uploadResult.setName(image.getName());
            message.reply(uploadResult.toString());
        });
    }

    private void readImage(Message<String> message) {
        String imageId = message.body();
        client.find("image", new JsonObject().put("_id", imageId), event -> {
            List<JsonObject> result = event.result();
            if (result.isEmpty()) {
                message.reply(ImageFactory.notFound().toString());
                return;
            }
            message.reply(result.get(0).toString());
        });
    }
}
