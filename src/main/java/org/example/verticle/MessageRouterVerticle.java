package org.example.verticle;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import org.example.model.MessageData;


public class MessageRouterVerticle extends AbstractVerticle {
    @Override
    public void start() {
        vertx.eventBus().consumer("router", this::router);
    }

    private void router(Message<String> message) {
        if (message.body() != null && !message.body().isEmpty()) {
            System.out.println("Router message: " + message.body());
            MessageData data = Json.decodeValue(message.body(), MessageData.class);
            System.out.println(data);
            vertx.eventBus().send("/token/" + data.getAddress(), message.body());
            vertx.eventBus().send("database.save", message.body());
        }
    }
}
