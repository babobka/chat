package org.example.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.vertx.core.json.Json;

/**
 * Created by 123 on 25.03.2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Image {

    public enum Status {
        OK, NOT_FOUND
    }

    private Status status;
    private String name;
    private String contentType;
    private String base64Data;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getBase64Data() {
        return base64Data;
    }

    public void setBase64Data(String base64Data) {
        this.base64Data = base64Data;
    }

    @Override
    public String toString() {
        return Json.encodePrettily(this);
    }
}
