package org.example.model;

import org.apache.tika.Tika;

/**
 * Created by 123 on 25.03.2018.
 */
public class ImageFactory {
    private static final Tika TIKA = new Tika();

    public static Image notFound() {
        Image image = new Image();
        image.setStatus(Image.Status.NOT_FOUND);
        return image;
    }

    public static Image ok(String name, String base64Data) {
        Image image = new Image();
        image.setName(name);
        image.setBase64Data(base64Data);
        image.setStatus(Image.Status.OK);
        image.setContentType(TIKA.detect(name));
        return image;
    }
}
