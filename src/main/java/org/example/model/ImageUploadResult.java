package org.example.model;

import io.vertx.core.json.Json;

/**
 * Created by 123 on 27.03.2018.
 */
public class ImageUploadResult {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return Json.encodePrettily(this);
    }
}
