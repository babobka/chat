package org.example.model;

import io.vertx.core.json.Json;

public class MessageData {
    private String address;
    private String text;
    private ImageUploadResult imageUploadResult;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ImageUploadResult getImageUploadResult() {
        return imageUploadResult;
    }

    public void setImageUploadResult(ImageUploadResult imageUploadResult) {
        this.imageUploadResult = imageUploadResult;
    }

    @Override
    public String toString() {
        return Json.encodePrettily(this);
    }

}
